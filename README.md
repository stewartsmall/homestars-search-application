# HomeStars Search Application

This is a simple Application that allows a user to search for services related to home renovations and repairs. To execute this app simply download and unzip the repository, navigate to the repository folder in a terminal and run the commands below.

To start the service that serves the companies.json file, run the following command from the root directory of the repository:

- `json-server --watch ./src/data/companies.json`

Then run the following commands:

- `npm install`
- `npm start`
