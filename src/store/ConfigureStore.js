import * as reducers from "../reducers";
import { createStore, applyMiddleware, compose, combineReducers } from "redux";
import thunk from "redux-thunk";

const middleWare = compose(
  applyMiddleware(thunk),
  window.devToolsExtension ? window.devToolsExtension() : f => f
);
const rootReducer = combineReducers(reducers);
const store = createStore(rootReducer, middleWare);

export default store;
