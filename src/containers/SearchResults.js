import React, { Component } from "react";
import { connect } from "react-redux";
import { getAllCompanies } from "../actions/companyActions";
import { FontIcon, Paper } from "react-md";
import Pagination from "react-js-pagination";
import { Link } from "react-router-dom";

class SearchResults extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activePage: 1,
      pageEnd: 20,
      pageStart: 0
    };

    this.handlePageChange = this.handlePageChange.bind(this);
  }

  componentDidMount() {
    let { dispatch, getAllCompanies } = this.props;
    dispatch(getAllCompanies());
  }

  handlePageChange(pageNumber) {
    console.log(`active page is ${pageNumber}`);
    let pageStartValue = pageNumber > 1 ? pageNumber * 20 - 20 : 0;
    let pageEndValue = pageNumber * 20;

    this.setState({
      activePage: pageNumber,
      pageEnd: pageEndValue,
      pageStart: pageStartValue
    });
  }

  render() {
    const { companies } = this.props;
    const { pageStart, pageEnd } = this.state;

    if (!companies) {
      return <div>Loading...</div>;
    }

    return (
      <section className="company-list">
        {companies.records.slice(pageStart, pageEnd).map(company => (
          <section key={company.id}>
            <Paper className="md-cell md-cell--12 md-grid white">
              <section className="md-cell md-cell--3-tablet md-cell--2-desktop">
                <div className="company-result-image">
                  <img
                    className="company-result-image__image"
                    src={company.logo}
                    alt="company logo"
                  />
                </div>
              </section>
              <section className="md-cell md-cell--5-tablet md-cell--10-desktop">
                <h3>{company.name}</h3>
                <p>
                  <FontIcon>star</FontIcon>
                  {Math.round((company.avg_rating_cache / 10) * 100)}%
                  <br />
                  {company.services}
                  <br />
                  <Link to={`/company/${company.id}`}>View More &raquo;</Link>
                </p>
              </section>
            </Paper>
          </section>
        ))}
        <div className="center">
          <Pagination
            activePage={this.state.activePage}
            itemsCountPerPage={20}
            totalItemsCount={companies.count}
            pageRangeDisplayed={5}
            onChange={this.handlePageChange}
          />
        </div>
      </section>
    );
  }
}

function mapStateToProps(state) {
  return {
    companies: state.companies,
    getAllCompanies: getAllCompanies
  };
}

export default connect(mapStateToProps)(SearchResults);
