import React, { Component } from "react";
import { Toolbar } from "react-md";
import reno from "../assets/images/home-reno.png";

class Header extends Component {
  handleOnClick = () => {
    window.location.href = "/";
  };

  render() {
    return (
      <Toolbar
        className="link"
        colored
        title={<img src={reno} alt="logo" height="60px" />}
        onClick={this.handleOnClick}
      />
    );
  }
}

export default Header;
