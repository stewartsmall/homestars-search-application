import React, { Component } from "react";
import { orderFilter } from "./assets/FilterFields";
import { SelectField, TextField, FontIcon } from "react-md";
import _ from "lodash";
import { connect } from "react-redux";
import { sortedCompanies, searchCompanies } from "../actions/companyActions";

class Filters extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchTerm: "",
      persistSort: { name: "", order: "" }
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEnterKeyPress = this.handleEnterKeyPress.bind(this);
    this.executeSearch = this.executeSearch.bind(this);
    this.sortResultsBy = this.sortResultsBy.bind(this);
    this.performSort = this.performSort.bind(this);
  }

  handleChange(term) {
    this.setState({
      searchTerm: term
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.executeSearch();
  }

  handleEnterKeyPress(event) {
    if (event.keyCode === 13) {
      this.executeSearch();
    }
  }

  sortResultsBy(value) {
    let filterOption = value;

    switch (filterOption) {
      case "alphaAsc":
        this.performSort("name", "asc");
        break;

      case "alphaDesc":
        this.performSort("name", "desc");
        break;

      case "ratingAsc":
        this.performSort("avg_rating_cache", "asc");
        break;

      case "ratingDesc":
        this.performSort("avg_rating_cache", "desc");
        break;

      default:
        return;
    }
  }

  performSort(name, order) {
    let { companies, dispatch, updateCompanies } = this.props;
    let sortedArray = _.orderBy(companies.records, name, order);
    dispatch(updateCompanies(sortedArray));
    this.setState(
      {
        persistSort: { name: name, order: order }
      },
      () => console.log(this.state.persistSort)
    );
  }

  executeSearch() {
    let { dispatch, searchCompanies } = this.props;
    let { searchTerm, persistSort } = this.state;
    dispatch(searchCompanies(searchTerm, persistSort));
  }

  render() {
    return (
      <section>
        <h3>Search</h3>
        <div className="md-grid">
          <TextField
            id="text-with-close-button"
            label="Search Contractors"
            rightIcon={<FontIcon onClick={this.handleSubmit}>search</FontIcon>}
            className="md-cell md-cell--bottom"
            onChange={this.handleChange}
            onKeyDown={this.handleEnterKeyPress}
            helpText="This search field searches within the categories"
          />

          <SelectField
            id="select-field-1"
            label="Filter Results"
            placeholder="Placeholder"
            className="md-cell"
            menuItems={orderFilter}
            onChange={this.sortResultsBy}
            helpText="Filter the results below with these options"
          />
        </div>
      </section>
    );
  }
}

function mapStateToProps(state) {
  return {
    companies: state.companies,
    updateCompanies: sortedCompanies,
    searchCompanies: searchCompanies
  };
}

export default connect(mapStateToProps)(Filters);
