export const orderFilter = [
  {
    label: "Alphabetically Ascending",
    value: "alphaAsc"
  },
  {
    label: "Alphabetically Descending",
    value: "alphaDesc"
  },
  {
    label: "Ratings: Low - High",
    value: "ratingAsc"
  },
  {
    label: "Ratings: High - Low",
    value: "ratingDesc"
  }
];
