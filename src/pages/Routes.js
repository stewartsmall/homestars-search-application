import React from "react";
import { Switch, Route } from "react-router-dom";
import Home from "../pages/Home";
import Company from "../pages/Company";

const Routes = () => (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route path="/company/:id" component={Company} />
  </Switch>
);

export default Routes;
