import React from "react";
import Filters from "../containers/Filters";
import SearchResults from "../containers/SearchResults";
import { Paper, Grid } from "react-md";

const Home = () => (
  <section>
    <Grid>
      <Paper key={1} zDepth={1} raiseOnHover={1 === 0} className="search-bar">
        <Filters />
      </Paper>
    </Grid>

    <br />

    <Grid className="search-results">
      <SearchResults />
    </Grid>
  </section>
);

export default Home;
