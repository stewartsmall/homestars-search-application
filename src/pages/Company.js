import React, { Component } from "react";
import { connect } from "react-redux";
import { getSingleCompany } from "../actions/companyActions";
import { Card, CardTitle, CardText } from "react-md";

class Company extends Component {
  componentDidMount() {
    let id = this.props.match.params.id;
    let { dispatch, getSingleCompany } = this.props;
    dispatch(getSingleCompany(id));
  }

  render() {
    let { company } = this.props;

    if (!company) {
      return <div>Loading...</div>;
    }

    let percentRatings =
      Math.round((company.avg_rating_cache / 10) * 100) + "%";

    return (
      <div className="md-grid company-profile">
        <Card className="md-cell md-cell--12">
          <div className="md-grid">
            <div className="md-cell md-cell--2">
              <img src={company.logo} alt="logo" className="logo-img" />
            </div>
            <div className="md-cell md-cell--8">
              <CardTitle title={company.name} subtitle={company.services} />
            </div>
            <div className="md-cell md-cell--2">
              <CardTitle title={percentRatings} subtitle="Ratings" />
            </div>
          </div>
          <CardText>
            <h5>PROFILE</h5>
            <p>{company.profile}</p>
          </CardText>
        </Card>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    company: state.companies,
    getSingleCompany: getSingleCompany
  };
}

export default connect(mapStateToProps)(Company);
