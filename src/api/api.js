import { create } from "apisauce";

const api = create({
  baseURL: "http://localhost:3000/companies/"
});

export default api;
