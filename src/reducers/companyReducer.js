import {
  ALL_COMPANIES,
  GET_COMPANY,
  SORTED_COMPANIES,
  SEARCH_COMPANIES
} from "../actions/companyActions";

function companyReducer(state = null, action) {
  switch (action.type) {
    case ALL_COMPANIES:
      return action.payload;

    case GET_COMPANY:
      return action.payload;

    case SORTED_COMPANIES:
      return action.payload;

    case SEARCH_COMPANIES:
      return action.payload;

    default:
      return state;
  }
}

export default companyReducer;
