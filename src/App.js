import React from "react";
import Header from "./containers/Header";
import Routes from "./pages/Routes";

const App = () => (
  <div className="App">
    <Header />
    <Routes />
  </div>
);

export default App;
