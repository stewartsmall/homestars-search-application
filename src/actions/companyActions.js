import api from "../api/api";
import _ from "lodash";

export const ALL_COMPANIES = "ALL_COMPANIES";
export const GET_COMPANY = "GET_COMPANY";
export const SORTED_COMPANIES = "SORTED_COMPANIES";
export const SEARCH_COMPANIES = "SEARCH_COMPANIES";

// Action creator to get all companies
export function getAllCompanies() {
  const request = api.get("/");

  return dispatch => {
    request.then(({ data }) => {
      dispatch({
        type: ALL_COMPANIES,
        payload: { records: data, count: data.length }
      });
    });
  };
}

// Action creator to get a company
export function getSingleCompany(id) {
  const request = api.get(`/${id}`);

  return dispatch => {
    request.then(({ data }) => {
      dispatch({
        type: GET_COMPANY,
        payload: data
      });
    });
  };
}

// Action creator to switch companies to a sorted array
export function sortedCompanies(sortedCompanies) {
  return {
    type: SORTED_COMPANIES,
    payload: { records: sortedCompanies, count: sortedCompanies.length }
  };
}

// Action creator to search companies based on a search term
export function searchCompanies(searchTerm, persistSort) {
  const request = api.get("/");
  let searchResults = [];

  console.log(persistSort);

  return dispatch => {
    request.then(({ data }) => {
      if (searchTerm !== "") {
        searchResults = data.filter(
          company => company.categories_info_cache.indexOf(searchTerm) > -1
        );
        if (persistSort.name !== "" && persistSort.order !== "") {
          searchResults = reSortArray(
            searchResults,
            persistSort.name,
            persistSort.order
          );
        }
      } else {
        if (persistSort.name !== "" && persistSort.order !== "") {
          searchResults = reSortArray(
            data,
            persistSort.name,
            persistSort.order
          );
        }
      }

      dispatch({
        type: SEARCH_COMPANIES,
        payload: { records: searchResults, count: searchResults.length }
      });
    });
  };
}

function reSortArray(array, name, order) {
  return _.orderBy(array, name, order);
}
