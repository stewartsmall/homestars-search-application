import React from "react";
import { FontIcon } from "react-md";

const HomeIcon = () => <FontIcon>home</FontIcon>;

export default HomeIcon;
